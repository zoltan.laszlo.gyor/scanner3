package main;

import java.util.Scanner;
import java.io.*;

public class Main {
	public static void main(String[] args) {
		try{
		      File file = new File("input.txt");
		      Scanner sc = new Scanner(file);
		      int countWordO = 0;
		      int countWordR = 0;
		      while(sc.hasNextLine()){
		        String line = sc.nextLine();
		        System.out.println(line);
		        for(int i = 0; i<line.length(); i++){
		          if(line.charAt(i) == 'o' || line.charAt(i) == 'O'){
		            countWordO++;
		          }
		          if(line.charAt(i) == 'r' || line.charAt(i) == 'R'){
		            countWordR++;
		          }
		        }
		      }
		      System.out.println("Number of o in the text: " + countWordO);
		      System.out.println("Number of r in the text: " + countWordR);
		      if(countWordR > countWordO){
		        System.out.println("There are more R's in the text than O's by: " + (countWordR-countWordO));
		      }else{
		        System.out.println("There are more O's in the text than R's by: " + (countWordO-countWordR));
		      }
		    }catch(FileNotFoundException e){
		      System.out.println("what am I supposed to do?");
		    }
	}
}
